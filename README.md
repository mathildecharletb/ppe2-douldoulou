# PPE2: Analyse de l'Actualité 2022 par Topic Modeling

Ici le site : [Site](https://lilasp.gitlab.io/ppe2-douldoulou.gitlab.io/index.html)

## Introduction

Le projet PPE2 vise à analyser les sujets et expressions qui ont marqué l'actualité en 2022 dans les publications du journal "Le Monde". Grâce à des techniques de topic modeling, nous explorons et visualisons les thématiques principales de différentes périodes de l'année afin de comprendre l'évolution et la répartition des sujets d'actualité.

## Composition de l'Équipe

- **Mathilde Charlet** - [GitLab](https://gitlab.com/mathildecharletb)
- **Diego Rossini** - [GitLab](https://gitlab.com/diego.rossini418)
- **Lilas Pastré** - [GitLab](https://gitlab.com/Lpastre)

## Objectifs

1. **Constituer un corpus multilingue** de l'actualité 2022 focalisé sur des périodes particulièrement marquantes.
2. **Analyser et visualiser ces corpus** à l'aide de techniques de topic modeling, pour extraire et rendre visibles les thèmes principaux de l'année.
3. **Produire des visualisations** pertinentes et accessibles qui permettent de tirer des conclusions sur les sujets dominants et leur évolution temporelle.

## Méthodologie et Étapes

### Initialisation du Projet

- Maintien du groupe formé de Lilas Pastré, Diego Rossini et Mathilde Charlet.
- Initiation à GitLab et exercices pour maîtriser Git.

### Création du Corpus

- Création du repository GitLab : **PPE2-Douldoulou**.
- Constitution de branches individuelles et fusion dans un corpus commun.

### Traitement des Données

- Développement de scripts pour extraire les lexiques, compter les occurrences de mots et effectuer le tri des articles par catégories.
- Utilisation de Pathlib et Feedparser pour manipuler les fichiers XML et intégrer les résultats.

### Analyse et Visualisation

- Construction de scripts pour le topic modeling avec `run_lda.py`, permettant de générer des visualisations des sujets principaux.
- Utilisation de Spacy, Stanza et Trankit pour l'analyse des textes.
- Test et validation des visualisations avec PyLDAvis pour examiner la cohérence et l'utilité des topics générés.

### Focus sur les Élections Présidentielles

- Définition de périodes clés autour des élections présidentielles françaises (avant, pendant et après les élections).
- Génération de corpus spécifiques pour ces périodes et leur analyse ciblée.

### Finalisation et Présentation

- Sélection des visualisations les plus pertinentes et intégration au site web.
- Écriture d'articles explicatifs sur les scripts et les résultats d'analyse, intégration sur le site.

## Conclusion

Le projet PPE2 a permis de réaliser une analyse poussée des sujets qui ont marqué l'actualité en 2022, en utilisant des méthodes avancées de topic modeling. Les résultats ont été visualisés et présentés de manière accessible sur un site web dédié, permettant ainsi de tirer des conclusions intéressantes sur l'évolution des thèmes d'actualité au cours de l'année.

Pour toute demande de collaboration ou d'information supplémentaire, n'hésitez pas à nous contacter.

Merci pour votre intérêt!

**L'équipe du projet PPE2**