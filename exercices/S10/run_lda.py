r"""
LDA Model
=========

Introduces Gensim's LDA model and demonstrates its use on the NIPS corpus.

"""

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel
from pprint import pprint
from xml.etree import ElementTree as ET
import argparse
import sys
import glob

def from_xml_to_lemma_list(corpus, word_or_lemma, pos_filtre):
    #Comme vient chercher la liste des lemmes déjà parsé dans nos fichier xml
    for fichier in glob.glob(corpus+'/*.xml'):
        with open(fichier, "r") as f:
            xml = ET.parse(f)
            docs=[]
            for article in xml.findall("//analyse"):
                doc = []
                for token in article.findall("./token"):
                    if pos_filtre !="":
                        if token.attrib['pos'] in pos_filtre:
                            doc.append(token.attrib[word_or_lemma]/token.attrib['pos'])
                    else:
                        doc.append(token.attrib[word_or_lemma]/token.attrib['pos'])

            if len(doc) > 0:
                docs.append(doc)
    return docs

def from_json_to_lemma_list(c):
    print("à construire")

def from_pickle_to_lemma_list(c):
    print("à construire")


def run_lda(docs):


    # Compute bigrams.
    # from gensim.models import Phrases

    # Add bigrams and trigrams to docs (only ones that appear 20 times or more).
    bigram = Phrases(docs, min_count=20)
    for idx in range(len(docs)):
        for token in bigram[docs[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                docs[idx].append(token)


    # Remove rare and common tokens.
    # from gensim.corpora import Dictionary

    # Create a dictionary representation of the documents.
    dictionary = Dictionary(docs)

    # Filter out words that occur less than 20 documents, or more than 50% of the documents.
    dictionary.filter_extremes(no_below=20, no_above=0.5) #ces deux variables vont être des arguments

    # Bag-of-words representation of the documents.
    corpus = [dictionary.doc2bow(doc) for doc in docs]


    print('Number of unique tokens: %d' % len(dictionary))
    print('Number of documents: %d' % len(corpus))



    # Train LDA model.
    # from gensim.models import LdaModel

    # Set training parameters.
    num_topics = 10 #Cette var deviendra également un argument
    chunksize = 2000
    passes = 20
    iterations = 400 #Cette var deviendra également un argument
    eval_every = None  # Don't evaluate model perplexity, takes too much time.

    # Make an index to word dictionary.
    temp = dictionary[0]  # This is only to "load" the dictionary.
    id2word = dictionary.id2token

    model = LdaModel(
        corpus=corpus,
        id2word=id2word,
        chunksize=chunksize,
        alpha='auto',
        eta='auto',
        iterations=iterations,
        num_topics=num_topics,
        passes=passes,
        eval_every=eval_every
    )


    top_topics = model.top_topics(corpus)

    # Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
    avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
    print('Average topic coherence: %.4f.' % avg_topic_coherence)

    # from pprint import pprint
    pprint(top_topics)

if __name__ == "__main__":
    #TODO : ajouter en argument les differentes variables du code
    parser = argparse.ArgumentParser()
    # parser.add_argument("-b", help="no_below variable. Filter out words that occur less than n documents", required=True)
    # parser.add_argument("-a", help="no_above variable. 0<a<1. Filter out words that occur more than n\% of the document", required=True)
    parser.add_argument("-p", help="a list of pos, to filter the results.", default="")
    parser.add_argument("-t", help="choose between form tokenization or lemma", default="lemme")
    parser.add_argument("-f", help="input format")
    parser.add_argument("corpus_dir", help="root dir of the corpus data")
    # parser.add_argument("categories",nargs="*", help="catégories à retenir")
    args = parser.parse_args()
    if args.f == 'xml':
        docs = from_xml_to_lemma_list(args.corpus_dir, args.t, args.p)
    elif args.f == 'json':
        docs = from_json_to_lemma_list(args.corpus_dir, args.t, args.p)
    elif args.f == 'pickle':
        docs = from_pickle_to_lemma_list(args.corpus_dir, args.t, args.p) 
    else:
        print("méthode non disponible", file=sys.stderr)
        sys.exit()
    
    run_lda(docs)