from pathlib import Path

MONTHS = ["Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug", 
          "Sep",
          "Oct",
          "Nov", 
          "Dec"]

DAYS = [f"{x:02}" for x in range(1,32)]

def rec_walk(chemin:Path,mois:str,jour:str, do_something):
    if chemin.is_dir():
        if chemin.name in MONTHS:
            mois = chemin.name
        if chemin.name in DAYS:
            jour = chemin.name
        for item in chemin.iterdir():
            rec_walk(item, mois, jour, do_something)
    else:
        if chemin.name.endswith(".xml"):
            do_something( mois, jour, chemin.name)


if __name__ == "__main__":
    print("start")
    rec_walk(Path("../Corpus/2022"), "","", print)

