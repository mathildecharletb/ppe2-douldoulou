import spacy
import trankit
import stanza
from datastructures import Token


def spacy_analyse(article):
    nlp = spacy.load("fr_core_news_lg")
    text = (article.titre or "" ) + "\n" + (article.description or "")
    doc = nlp(text)
    for token in doc:
        t = Token(token.text, token.lemma_, token.pos_)
        article.analyse.append(t)
    return article


def trankit_analyse(article):
    p = trankit.Pipeline('french')
    result = p((article.titre or "" ) + "\n" + (article.description or ""))
   
    output = []
    for sentence in result['sentences']:
        for token in sentence['tokens']:
            if 'expanded' not in token.keys():
                token['expanded'] = [token]
            for w in token['expanded']:
                output.append(Token(w['text'], w['lemma'], w['upos']))
    article.analyse = output
    return article



def stanza_analyse(article):
    stanza.download('french')
    nlp = stanza.Pipeline('fr')
    doc = nlp((article.titre or "" ) + "\n" + (article.description or ""))

    for i, sent in enumerate(doc.sentences):
        for word in sent.words:
            t = Token(word.text, word.lemma, word.pos)
            article.analyse.append(t)
    return article
