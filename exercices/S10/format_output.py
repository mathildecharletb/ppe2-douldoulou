from datastructures import Corpus, Article, Token
from xml.etree import ElementTree as ET
from typing import List
import pickle
import json
from dataclasses import asdict


def write_json(corpus:Corpus, dest:str):
    with open(dest, 'a') as output_file:
        json.dump(asdict(corpus), output_file)


def write_pickle(corpus:Corpus, dest:str):
    with open(dest,'ab') as pickle_file:
        pickle.dump(corpus, pickle_file)


def analyse_to_xml(tokens: List[Token]) -> ET.Element:
    root = ET.Element("analyse")
    for tok in tokens:
        tok_element = ET.SubElement(root, "token")
        tok_element.attrib['forme'] = tok.forme
        tok_element.attrib['pos'] = tok.pos
        tok_element.attrib['lemme'] = tok.lemme
    return root


def article_to_xml(article: Article) -> ET.Element:
    art = ET.Element("article")
    #art.attrib['date'] = article.date
    #art.attrib['categorie'] = article.categorie
    title = ET.SubElement(art, "title")
    description = ET.SubElement(art, "description")
    title.text = article.titre
    description.text = article.description
    art.append(analyse_to_xml(article.analyse))
    return art

def write_xml(corpus: Corpus, destination: str):
    root = ET.Element("corpus")
    root.attrib['begin'] = corpus.begin
    root.attrib['end'] = corpus.end
    cat = None
    if len(corpus.categorie) > 1:
        cat = ", ".join(corpus.categorie)
    else:
        cat = corpus.categorie[0]
    root.attrib["categories"] = cat
    # categories = ET.SubElement(root, "categories")
    #for c in corpus.categorie:
    #   ET.SubElement(categories, "cat").text = c
    content = ET.SubElement(root, "content")
    for article in corpus.articles:
        art_xml = article_to_xml(article)
        content.append(art_xml)
    tree = ET.ElementTree(root)
    ET.indent(tree)
    tree.write(destination)
