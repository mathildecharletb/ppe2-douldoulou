#!/usr/bin/env python
# coding: utf-8
# %%

# %%


import os
import sys
from nltk.tokenize import word_tokenize
import sys
import fileinput


def lire_corpus():
    """construit une liste de chaînes où chaque chaîne correspondra au contenu texte d’un fichier du dossier Corpus"""
    liste_str = []  # On crée la liste
    # et pour chaque nom de fichier dans corpus
    for filename in os.listdir(os.path.join(os.getcwd(), "Corpus")):
        # on ouvre le fichier en mode lecture
        with open(os.path.join(os.getcwd(), "Corpus", filename), 'r', encoding="utf8") as mon_fichier:
            # et on stocke le texte comme chaîne de caractère dans la liste
            liste_str.append(mon_fichier.read())
    return(liste_str)  # On retourne la liste


def lire_stdin_liste_tp2_r3():
    """Permet de lister les fichiers du corpus sur l'entrée standard"""
    for line in fileinput.input(encoding="utf-8"):
        print(line)


def fichier_occurrence(liste_str):
    """Prend comme argument une liste de chaînes et retourne un dictionnaire qui associe chaque mot à son nombre d’occurrences dans le corpus."""
    dic = {}  # On crée le dictionnaire
    for article in liste_str:  # Pour chaque chaîne dans la liste
        mots = word_tokenize(article)  # on tokenise la chaîne en mots
        for mot in mots:
            # Si le mot apparaît dans le dictionnaire, on ajoute +1 à la valeur
            if mot in dic:
                dic[mot] += 1
            # sinon, on crée l'entrée du mot dans le dictionnaire avec la valeur 1
            else:
                dic[mot] = 1
    return(dic)  # On retourne le dictionnaire


def fichiers_list_str_TP2():
    """permet la lecture du corpus comme une liste de fichiers en argument sur la ligne de commande"""
    idx = 1 #Variable pour afficher la numérotation des articles
    for filename in sys.argv[1:]: #On lit la liste de fichiers txt donnée en argument sur la ligne de commande
            with open(filename) as infile:
                print("ARTICLE NUMERO : ",idx, "\n", infile.read(), "\n\n\n\n\n") #On affiche le contenu de l'article
                idx += 1




def fichier_occurrence(liste_str):
    """Prend comme argument une liste de chaînes et retourne un dictionnaire qui associe chaque mot à son nombre d’occurrences dans le corpus."""
    dic = {}  # On crée le dictionnaire
    for article in liste_str:  # Pour chaque chaîne dans la liste
        mots = word_tokenize(article)  # on tokenise la chaîne en mots
        for mot in mots:
            # Si le mot apparaît dans le dictionnaire, on ajoute +1 à la valeur
            if mot in dic:
                dic[mot] += 1
            # sinon, on crée l'entrée du mot dans le dictionnaire avec la valeur 1
            else:
                dic[mot] = 1
    return(dic)  # On retourne le dictionnaire

#nombre de documents dans lequel le mot apparait


def occurrences_docs(liste_str=list):
    """Prend comme argument une liste de chaînes et retourne un dictionnaire qui compte le nombre d'articles dans lesquels un mot unique apparait"""
    dic = {}  # On crée le dictionnaire
    for article in liste_str:  # Pour chaque chaîne dans la liste

        # tokenisation et liste de mots uniques
        mots_article = set(word_tokenize(article))

        # on check que le mot unique est (ou pas) dans le dictionnaire
        for mot in mots_article:
            if mot in dic.keys():
                dic[mot] += 1
            else:
                dic[mot] = 1
    return dic



def main():
    # corpus = lire_corpus()
    # print("doc freq")
    # for k, v in occurrences_docs(corpus).items():
    #     print(f"{k}: {v}")
    # print("term freq")
    # for k, v in fichier_occurrence(corpus).items():
    #     print(f"{k}: {v}")
    if sys.argv[1:]:
        fichiers_list_str_TP2()
    else:
        # appel de la fonction r2
        lire_stdin_liste_tp2_r3()


if __name__ == "__main__":
    main()

