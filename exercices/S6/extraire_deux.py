#!/usr/bin/env python
# coding: utf-8
# %%


from pathlib import Path
import glob
import feedparser
import argparse


# %%


path_home = "./Corpus/"


# %%


path = Path(path_home)


# %%


from extraire_un import extraction_titre_description


# %%


dico_cat={"une":"0,2-3208,1-0,0", 
          "international":"0,2-3210,1-0,0", 
          "europe":"0,2-3214,1-0,0",
          "societe":"0,2-3224,1-0,0", 
          "idees":"0,2-3232,1-0,0",
          "economie":"0,2-3234,1-0,0",
          "actualite-medias":"0,2-3236,1-0,0",
          "sport":"0,2-3242,1-0,0",
          "planete":"0,2-3244,1-0,0",
          "culture":"0,2-3246,1-0,0",
          "livres":"0,2-3260,1-0,0",
          "cinema":"0,2-3476,1-0,0",
          "voyage":"0,2-3546,1-0,0",
          "technologies":"0,2-651865,1-0,0",
          "politique":"0,57-0,64-823353,0",
          "sciences":"env_sciences"}


# %%
def get_keys_from_value(d, val):
    return [k for k, v in d.items() if v == val]


# %%
parser = argparse.ArgumentParser()
parser.add_argument('corpus', help='dossier contenant tous les fichiers XML')
parser.add_argument('cat', type=str, nargs='?',help='inserer une catégorie')
arguments, unknown = parser.parse_known_args()
#arguments = parser.parse_args()

# %%
with open("./GitPugnetta/output_ex2.xml","w") as file:
    file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    file.write(f'<categorie>{get_keys_from_value(dico_cat,arguments.cat)}\n\t')
    
    for file_path in path.glob("**/*.xml"):
        
        
        d = feedparser.parse(file_path)
        cat = file_path.name[:-4]
        #cat = get_keys_from_value(dico_cat,cat)

        if arguments.cat == cat:
            for entries in d.entries: 
                    try:
                        dico = extraction_titre_description(file_path)
                        idx = 0
                        while len(dico["titres"]) > idx:
                            file.write(f'<titre>{dico["titres"][idx]}</titre>\n\t')
                            file.write(f'<description>{dico["descriptions"][idx]}</descriptiion>\n\t')
                            idx += 1
                    except:
                        continue
    file.write('\n</categorie>')

