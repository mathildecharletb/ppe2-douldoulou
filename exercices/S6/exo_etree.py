import xml.etree.ElementTree as ET
import sys

def find_title_descr(file):

    articles={}
    c=0

    tree = ET.parse(file)
    root = tree.getroot()


    for article in root.iter('item'):
        c+=1
        try:
            title = article.find('title').text
            descr = article.find('description').text
            articles['Titre '+str(c)] = title
            articles['Description '+str(c)] = descr
        except:
            continue

    return articles


if __name__ == "__main__":
    find_title_descr()