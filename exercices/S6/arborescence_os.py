import os
from extraire_un import recupTxt
import argparse

def parcoursCorpus(categories):
    # on donne ici le dictionnaire avec les codes correspondants aux catégories
    dico_cat = {"une": "0,2-3208,1-0,0",
                "international": "0,2-3210,1-0,0",
                "europe": "0,2-3214,1-0,0",
                "societe": "0,2-3224,1-0,0",
                "idees": "0,2-3232,1-0,0",
                "economie": "0,2-3234,1-0,0",
                "actualite-medias": "0,2-3236,1-0,0",
                "sport": "0,2-3242,1-0,0",
                "planete": "0,2-3244,1-0,0",
                "culture": "0,2-3246,1-0,0",
                "livres": "0,2-3260,1-0,0",
                "cinema": "0,2-3476,1-0,0",
                "voyage": "0,2-3546,1-0,0",
                "technologies": "0,2-651865,1-0,0",
                "politique": "0,57-0,64-823353,0",
                "sciences": "env_sciences"}
    corpus = "../" + "2022"
    niv1 = os.listdir(os.path.join(corpus))
    for Month in niv1:
        if Month[0] != ".":
            day = os.listdir(os.path.join(corpus, Month))
            for d in day:
                if d[0] != ".":
                    hour = os.listdir(os.path.join(corpus, Month, d))
                    for h in hour:
                        if h[0] != ".":
                            files = os.listdir(os.path.join(corpus, Month, d, h))
                            for f in files:
                                #si nos fichiers se terminent par .xml et que nous n'avons pas de catégories indiquées, on affiche tout
                                if f.endswith("xml") and categories ==[]:
                                    print(Month,d)
                                    recupTxt(os.path.join(corpus, Month, d, h,f))
                                elif f.endswith("xml") and categories !=[]:
                                    #on a les catégories, on va donc récupérer les codes grâce au dictionnaire
                                    code=[]
                                    for i in range(len(categories)):
                                        code.append(dico_cat.get(categories[i]))
                                    for j in range(len(code)):
                                        if code[j] in str(f):
                                            print(Month,d)
                                            recupTxt(os.path.join(corpus, Month, d, h,f))

def main():
    parser=argparse.ArgumentParser()
    parser.add_argument("categories",help="Indiquer les catégories souhaitées", nargs="*")
    args=parser.parse_args()
    parcoursCorpus(args.categories)

if __name__ == "__main__":
    main()