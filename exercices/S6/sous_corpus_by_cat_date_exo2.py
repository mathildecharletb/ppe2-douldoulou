from exo_etree import find_title_descr
import sys
import argparse
import pathlib
from pathlib import Path
import glob

dico_cat={"une":"0,2-3208,1-0,0",
          "international":"0,2-3210,1-0,0",
          "europe":"0,2-3214,1-0,0",
          "societe":"0,2-3224,1-0,0",
          "idees":"0,2-3232,1-0,0",
          "economie":"0,2-3234,1-0,0",
          "actualite-medias":"0,2-3236,1-0,0",
          "sport":"0,2-3242,1-0,0",
          "planete":"0,2-3244,1-0,0",
          "culture":"0,2-3246,1-0,0",
          "livres":"0,2-3260,1-0,0",
          "cinema":"0,2-3476,1-0,0",
          "voyage":"0,2-3546,1-0,0",
          "technologies":"0,2-651865,1-0,0",
          "politique":"0,57-0,64-823353,0",
          "sciences":"env_sciences"}

dico_date={"Jan":"1",
           "Feb":"2",
           "Mar":"3",
           "Apr":"4",
           "May":"5",
           "Jun":"6",
           "Jul":"7",
           "Aug":"8",
           "Sep":"9",
           "Oct":"10",
           "Nov":"11",
           "Dec":"12"}


def make_corpus_by_date(path_to_corpus, date):
    month = str(date)[:-2]
    day = str(date)[-2:]
    for k, v in dico_date.items():
        if v == month:
            m=k
    path_home = Path(path_to_corpus+"/"+m+"/"+day+"/")
    with open("./output_ex2.xml","w") as file:
        file.write('<?xml version="1.0" encoding="UTF-8"?>\n<text>\n\t<body>\n\t\t')
        file.write(f'<date>{day} {m}</date>\n\t\t')
        for f in path_home.glob('**/*.xml'):
            articles = find_title_descr(f)
            for k, v in articles.items():
                if k.startswith('Titre'):
                    file.write(f'<titre>{v}</titre>\n\t\t')
                elif k.startswith('Description'):
                    file.write(f'<description>{v}</description>\n\t\t')
        file.write('\n\t</body>\n</text>')



def make_corpus_by_cat(path_to_corpus, cat):
    print("HEY cat = ", cat)


def redistribution():
    parser = argparse.ArgumentParser()
    parser.add_argument('fichier')
    parser.add_argument('date', metavar='date MMDD', type=int, nargs='?', help='a date')
    parser.add_argument('cat', metavar='cat', type=str, nargs='?', help='a category of articles')

    arguments = parser.parse_args()

    if arguments.date:
        make_corpus_by_date(arguments.fichier, arguments.date)
    if arguments.cat:
        make_corpus_by_cat(arguments.fichier, arguments.cat)




if __name__ == "__main__":
    if len(sys.argv) > 1:
        redistribution()
    else:
        print("Indiquez deux arguments : le chemin du corpus et la date (MMJJ) ou la catégorie.")