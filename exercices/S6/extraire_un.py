#!/usr/bin/env python
# coding: utf-8
# %%
import feedparser
import sys


# %%


def extraction_titre_description(xml_file_path):
    
    # Importation fichier XML et parsing
    d = feedparser.parse(xml_file_path)
    
    # Extraction titres
    titres = []
    for idx,title in enumerate(d.entries):
        #print(f'Article {idx+1} titre = {d.entries[idx].title}\n\n')
        titres.append(d.entries[idx].title)
    
    # Extraction descriptions
    descriptions = []
    for idx,title in enumerate(d.entries):
        #print(f'Article {idx+1} description = {d.entries[idx].summary}\n\n')
        descriptions.append(d.entries[idx].summary)
        
    dic_titres_descriptions = {'titres':titres,'descriptions':descriptions}  
    
    return dic_titres_descriptions


# %%



if __name__ == "__main__":
    if sys.argv:
        extraction_titre_description(sys.argv[1])
    else:
        print("Insérer un argument")

