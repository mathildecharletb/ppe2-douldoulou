from datastructures import Corpus, Article
from xml.etree import ElementTree as ET
import pickle
import json
from dataclasses import asdict


def write_json(corpus:Corpus, dest:str):
    with open(dest, 'a') as output_file:
        json.dump(asdict(corpus), output_file)


def write_pickle(corpus:Corpus, dest:str):
    with open(dest,'ab') as pickle_file:
        pickle.dump(corpus, pickle_file)



def article_to_xml(article: Article) -> ET.Element:
    art = ET.Element("article")
    title = ET.SubElement(art, "title")
    description = ET.SubElement(art, "description")
    title.text = article.titre
    description.text = article.description
    return art

def write_xml(corpus: Corpus, destination: str):
    root = ET.Element("corpus")
    root.attrib['begin'] = corpus.begin
    root.attrib['end'] = corpus.end
    cat = None
    if len(corpus.categorie) > 1:
        cat = ", ".join(corpus.categorie)
    else:
        cat = corpus.categorie[0]
    root.attrib["categories"] = cat
    content = ET.SubElement(root, "content")
    for article in corpus.articles:
        art_xml = article_to_xml(article)
        print(art_xml)
        content.append(art_xml)
        print("content", content)
    tree = ET.ElementTree(root)
    ET.indent(tree)
    tree.write(destination)



# def article_to_xml(article: Article): #-> et.Element:
#     root = et.Element("article")
#     title = et.SubElement(root, "title")
#     desc = et.SubElement(root, "description")
#     ana = et.SubElement(root,"analyse")
#     form = et.SubElement(ana,"token_form")
#     lemma = et.SubElement(ana,"lemma_token")
#     pos = et.SubElement(ana,"pos_token")
    
#     form.text = article.analyse[0]
#     lemma.text = article.analyse[1]
#     pos.text = article.analyse[2]
#     title.text = article.titre
#     desc.text = article.description
    
#     return root



# def write_xml(corpus: Corpus, dest:str):

#     root = et.Element("corpus")
#     root.attrib['begin'] = corpus.begin
#     root.attrib['end'] = corpus.end
#     cat = None
#     if len(corpus.categorie) > 1:
#         cat = ", ".join(corpus.categorie)
#     else:
#         cat = corpus.categorie[0]
#     root.attrib["categories"] = cat
#     content = et.SubElement(root, "article")
#     titre = et.SubElement(content, "titre")
#     desc = et.SubElement(content, "description")
#     ana = et.SubElement(content, "analyse")
#     token = et.SubElement(ana, "token")

#     for article in corpus.articles:
#         art_xml = article_to_xml(article)
#         print(art_xml, end='\n\n')
#         titre.append(art_xml.title)
#         desc.append(art_xml.desc)
#         # TODO : récup les infos de la liste analyse:[Token]
#         token.attrib['form'] = art_xml.form
#         token.attrib['lemme'] = art_xml.lemma
#         token.attrib['pos'] = art_xml.pos
#         break
    
#     # tree = et.ElementTree.root
#     # et.indent(tree)
#     # tree.write(dest)
