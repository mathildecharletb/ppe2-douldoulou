import spacy
import trankit
import stanza
from datastructures import Token


def spacy_analyse(article):
    nlp = spacy.load("fr_core_news_lg")
    text = article.description
    doc = nlp(text)
    for token in doc:
        t = Token(token.text, token.lemma_, token.pos_)
        article.analyse.append(t)
    return article


def trankit_analyse(article):
    p = trankit.Pipeline('french')
    doc_text = p(article.description)
   
    max_idx = len(doc_text['sentences'])
    idx = 0
    
    while idx < max_idx:
        for dico_tokens in doc_text['sentences'][idx]:
             article.analyse.append(Token(form=dico_tokens['text'],
                        lemme=dico_tokens['lemma'],
                        pos=dico_tokens['upos']))
        idx += 1


def stanza_analyse(article):
    # stanza.download('french')
    nlp = stanza.Pipeline('fr')
    doc = nlp(article.description)

    for i, sent in enumerate(doc.sentences):
        for word in sent.words:
            t = Token(word.text, word.lemma, word.pos)
            article.analyse.append(t)
    return article

