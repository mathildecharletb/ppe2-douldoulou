# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# Ce script définit plusieurs fonctions qui ont pour but de retourner en output un objet "Article". L'objet "Article" contiendra le titre et le texte des articles du corpus Le Monde
# passé en entrée. Trois façons différentes de parcourir et de récupérer les informations contenues dans les fichiers contenants les articles sont proposées :

# 1) Avec l'emploi d'expréssions régulières ;
# 2) En utilisant le parseur "feedparser ;
# 3) En utilisant le parseur "etree".

# Une fonction "nettoyage" efface du document en entrée les chaines de caractères qui pourraient déranger l'extraction des informations avec des expressions régulières.

# --------------------------------------------------------------------------------------------------------------------------------------------------------------


# Importation des librairies de parcours dans le système d'exploitation et des parseurs
import sys
import re
from pathlib import Path
import feedparser
from xml.etree import ElementTree as et
from datastructures import Article
regex_item = re.compile("<item><title>(.*?)<\/title>.*?<description>(.*?)<\/description>")


# Définition des fonctions nécessaires à l'accès aux informations textuelles des articles du corpus Le Monde avec "re"
def nettoyage(texte):
    texte_net = re.sub("<!\[CDATA\[(.*?)\]\]>", "\\1", texte) 
    return texte_net

def extract_re(fichier_rss):
    with open(fichier_rss, "r") as input_rss:
        lignes = input_rss.readlines()
        texte = "".join(lignes)
        for m in re.finditer(regex_item, texte):
            titre_net = nettoyage(m.group(1))
            description_net = nettoyage(m.group(2))
            yield Article(titre_net, description_net, [])


# Définition de la fonction nécessaire à l'accès aux informations textuelles des articles du corpus Le Monde avec "feedparser"
def extract_feedparser(fichier_rss):
    feed = feedparser.parse(fichier_rss)
    for entry in feed['entries']:
        yield Article(entry['title'], entry['summary'], [])


# Définition de la fonction nécessaire à l'accès aux informations textuelles des articles du corpus Le Monde avec "etree"
def extract_et(fichier_rss):
    text = Path(fichier_rss).read_text()
    if len(text) > 0:
        xml = et.parse(fichier_rss)
        root = xml.getroot()
        for item in root.findall(".//item"):
            title = item.find("title").text
            desc = item.find("description").text
            date = item.find("pubDate").text
            yield Article(title, desc, [])

            

# Cette fonction a été écrite pour tester ce script. Nous la gardons car cela nous a été utile lors du test des fonctions écrites auparavant

#def main(fichier_rss, extract_fun):
#    for i, (titre, description) in enumerate(extract_fun(fichier_rss)):
#        print(i,titre)
#        print(description)
#        print("---")
#
#if __name__ == "__main__":
#    fichier_rss = sys.argv[1]
#    print("démo RE")
#    main(fichier_rss, extract_re)
#    print("démo feedparser")
#    main(fichier_rss, extract_feedparser)
#    print("démo etree")
#    main(fichier_rss, extract_et)