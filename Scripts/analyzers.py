# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# Ce script a pour but de définir trois fonctions qui prennent en entrée le texte d'un article et renvoyent une analyse des tokens présents dans le texte.
# Pour chaque token on extrait : 

# 1) Sa forme dans le texte ;
# 2) Son lemme ; 
# 3) Sa partie du discours.

# L'extraction de ces trois informations prend la structure et le format qui sont définis dans le script "datastructures" pour la classe "Token".

# --------------------------------------------------------------------------------------------------------------------------------------------------------------


# Importation de librairies d'analyse et de la classe "Token" présente dans "datastructures"
import spacy
import trankit
import stanza²
from datastructures import Token


# Utilisation de la librairie "spacy" pour analyser les titres et la description des articles en input
def spacy_analyse(article):
    nlp = spacy.load("fr_core_news_lg")
    text = (article.titre or "" ) + "\n" + (article.description or "")
    doc = nlp(text)
    for token in doc:
        t = Token(token.text, token.lemma_, token.pos_)
        article.analyse.append(t)
    return article


# Utilisation de la librairie "trankit" pour analyser les titres et la description des articles en input
def trankit_analyse(article):
    p = trankit.Pipeline('french')
    result = p((article.titre or "" ) + "\n" + (article.description or ""))

    output = []
    for sentence in result['sentences']:
        for token in sentence['tokens']:
            if 'expanded' not in token.keys():
                token['expanded'] = [token]
            for w in token['expanded']:
                output.append(Token(w['text'], w['lemma'], w['upos']))
    article.analyse = output
    return article


# Utilisation de la librairie "stanza" pour analyser les titres et la description des articles en input
def stanza_analyse(article):
    stanza.download('french')
    nlp = stanza.Pipeline('fr')
    doc = nlp((article.titre or "" ) + "\n" + (article.description or ""))

    for i, sent in enumerate(doc.sentences):
        for word in sent.words:
            t = Token(word.text, word.lemma, word.pos)
            article.analyse.append(t)
    return article
